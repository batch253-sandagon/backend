// Import Express package and create an Express router
const express = require("express");
const router = express.Router();

// Import userController and auth modules
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route and controller for adding/creating a product (Requires authentication and Admin access)
router.post("/add", auth.verify, async (req, res) => {
  try {
      // Call getCart function from userController with decoded user data
      const userData = auth.decode(req.headers.authorization);
      const result = await productController.addProduct(req.body, userData.isAdmin);
      res.status(201).json(result);
  } catch (err) {
      console.log(err);
      res.status(500).json("Internal Server Error");
  }
});

// Route and controller for retrieving all the products (Requires authentication and Admin access)
router.get("/all", auth.verify, async (req, res) => {
  try {
      // Decode user data from authorization header
      const userData = auth.decode(req.headers.authorization);

      // Call getAllProducts function from userController with decoded user data
      const result = await productController.getAllProducts(userData.isAdmin);
      res.status(200).json(result);
  } catch (err) {
      console.log(err);
      res.status(500).json("Internal Server Error");
  }
});

// Route and controller for retrieving all the active products
router.post("/", async (req, res) => {
    try {
        // Call getAllActive function from userController with decoded user data
        const result = await productController.getAllActive(req.body);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("Internal Server Error");
    }
});

// Route and controller for retrieving single/specific product
router.get("/:productId", async (req, res) => {
    try {
        // Call getProduct function from userController with decoded user data
        const result = await productController.getProduct(req.params.productId);
        res.status(200).json(result);
    } catch (err) {
        console.log(err);
        res.status(500).json("Internal Server Error");
    }
});

// Route and controller for updating single/specific product (Requires authentication and Admin access)
router.patch("/:productId", auth.verify, async (req, res) => {
    try {
      const userData = auth.decode(req.headers.authorization);

      // Call updateProduct function from userController with decoded user data
      const result = await productController.updateProduct(req.body, req.params.productId, userData.isAdmin);
      res.status(200).json(result);
    } catch (err) {
      console.log(err);
      res.status(500).json("Internal Server Error");
    }
  });

// Route and controller for archiving specific product (Requires authentication and Admin access)
router.patch("/:productId/archive", auth.verify, async (req, res) => {
    try {
      const userData = auth.decode(req.headers.authorization);
  
      // Call archiveProduct function from userController with decoded user data
      const result = await productController.archiveProduct(req.params.productId, userData.isAdmin);
  
      res.status(200).json(result);
    } catch (err) {
      console.log(err);
      res.status(500).json("Internal Server Error");
    }
  });

// Route and controller for buying an specific product (Requires authentication)
router.post("/:productId/buyNow", auth.verify, async (req, res) => {
    try {
      const userData = auth.decode(req.headers.authorization);

      // Call checkoutProduct function from userController with decoded user data
      const result = await productController.checkoutProduct(req.params.productId, req.body, userData);
      res.status(201).json(result);
    } catch (error) {
      console.error(error);
      res.status(500).json("Internal Server Error");
    }
  });

// Route and controller for add specific product to cart (Requires authentication)
router.patch("/:productId/addToCart", auth.verify, async (req, res) => {
    try {
      const userData = auth.decode(req.headers.authorization);

      // Call addToCart function from userController with decoded user data
      const result = await productController.addToCart(userData, req);
      res.status(201).json(result);
    } catch (err) {
      console.error(err);
      res.status(500).json("Internal Server Error");
    }
});

// Export the router
module.exports = router;