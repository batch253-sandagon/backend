// Import Mongoose package
const mongoose = require("mongoose");

// Define the product schema using Mongoose
const productSchema = mongoose.Schema({
    name: {
        type: String, 
        required: [true, "Product name is required"]
    },
    description: {
        type: String, 
        required: [true, "Description is required"]
    },
    category: {
        type: String, 
        required: [true, "Description is required"]
    },
    price: {
        type: Number, 
        default: 0
    },
    stock: {
        type: Number,
        default: 0
    },
    image: {
        type: Object,
        required: [true, "Image is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
});

// Export the Product model using the product schema
module.exports = mongoose.model("Product", productSchema);