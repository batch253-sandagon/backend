// console.log("Hello.");

let message = "";

// Login Function
function login(username, password, role) {
    if(username === "" || username === undefined || password === "" || password === undefined || role === "" || role === undefined) {
        return "Inputs must not empty"
    } else {
        switch (role) {
        case "admin":
            return "Welcome back to the class portal, admin!";
            break;
        case "teacher":
            return "Thank you for logging in, teacher!";
            break;
        case "student":
            return "Welcome to the class portal, student!";
            break;
        default:
            return "Role out of range.";
        };
    };
};

message = login();
console.log(message);

message = login("","password","admin");
console.log(message);

message = login("adminUser","","admin");
console.log(message);

message = login("adminUser","password","");
console.log(message);

message = login("adminUser", "password", "admin");
console.log(message);

message = login("adminUser", "password", "teacher");
console.log(message);

message = login("adminUser", "password", "student");
console.log(message);

message = login("adminUser", "password", "carpenter");
console.log(message);

// checkAverage
function checkAverage(num1, num2, num3, num4){
    let average = (num1 + num2 + num3 + num4) / 4;
    average = Math.round(average);
    // console.log(average);

    if (average <= 74) {
        return "Hello, student, your average is " + average + ". The letter equivalent is F";
      } else if (average >= 75 && average <= 79) {
        return "Hello, student, your average is " + average + ". The letter equivalent is D";
      } else if (average >= 80 && average <= 84) {
        return "Hello, student, your average is " + average + ". The letter equivalent is C";
      } else if (average >= 85 && average <= 89) {
        return "Hello, student, your average is " + average + ". The letter equivalent is B";
      } else if (average >= 90 && average <= 95) {
        return "Hello, student, your average is " + average + ". The letter equivalent is A";
      } else {
        return "Hello, student, your average is " + average + ". The letter equivalent is A+";
      };
};

message = checkAverage(71, 70, 73, 71);
console.log(message);

message = checkAverage(75, 75, 76, 78);
console.log(message);

message = checkAverage(80, 82, 83, 81);
console.log(message);

message = checkAverage(85, 86, 85, 86);
console.log(message);

message = checkAverage(91, 90, 92, 90);
console.log(message);

message = checkAverage(95, 96, 97, 96);
console.log(message);

//Do not modify
//For exporting to test.js
try {
    module.exports = {
       login, checkAverage
    }
} catch(err) {

}