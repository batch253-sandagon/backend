const Course = require("../models/Course");

// Create a new course
/*
    Steps:
    1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
    2. Save the new Course to the database
*/
const addCourse = async (reqBody, isAdmin) => {
    try{
        if (isAdmin){
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            });
            const savedCourse = await newCourse.save();
            if(savedCourse){
                return true;
            } else {
                return false;
            };
        } else {
            return false;
        };
    } catch(err){
        return err
    }
};

const getAllCourses = async () => {
    try {
       const retrivedCourses = await Course.find({});
        return retrivedCourses; 
    } catch(err){
        return err;
    };
};

const getAllActive = async () => {
    try {
        const foundActives = await Course.find({isActive: true});
        return foundActives;
    } catch(err){
        return false;
    };
};

const getCourse = async (id) => {
    try {
        const course = await Course.findById(id);
        return course;
    } catch (err){
        return err;
    };
};

const updateCourse = async (reqBody, id) => {
    try {
        const course = await Course.findByIdAndUpdate(id, {
            "name": reqBody.name,
            "description": reqBody.description,
            "price": reqBody.price
        }, {new: true});
        return course;
    } catch (err){
        return err;
    };
};

const archiveCourse = async (reqBody, id) => {
    try {
        const course = await Course.findByIdAndUpdate(id, {
            "isActive": false
        });
        if(course){
            return true; 
        } else {
            return false;
        };
    } catch (err){
        return err;
    };
};

module.exports = {addCourse, getAllCourses, getAllActive, getCourse, updateCourse, archiveCourse};