const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
const userRoute = require("./routes/userRoute")
const courseRoute = require("./routes/courseRoute")

const app = express();
const port = 4000;
const db = mongoose.connection;

mongoose.connect("mongodb+srv://admin:admin123@b253-sandagon.ocfyhy9.mongodb.net/course-bookingAPI?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

db.once("open", ()=>console.log("Now connected to MongoDB Atlas."));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoute);
app.use("/courses", courseRoute);

if(require.main === module){
    app.listen(port, ()=>{
      console.log(`API is now online on ${process.env.PORT || 4000}`);  
    });
};

module.exports = app;