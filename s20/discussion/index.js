// console.log("Sup?");

// LOOPS

// While Loop
// let count = 5;

// while(count !== 0){
//     console.log("While: " + count);
//     count--;
// }

// let i = 1;

// while (i <= 15) {
//     console.log("Hi " + i);
//     i++;
// };

// for Loop

// for(let n = 0; n <= 20; n++){
//     console.log(n);
// }

// let myString = "IAmADeveloper";

// console.log(myString.length);

// for(i = 0; i < myString.length; i++){
//     console.log(myString[i]);
// }

const myName = "Ronald";
let vowels = 0;
for (let i = 0; i < myName.length; i++) {
  if (myName[i] === "a" || myName[i] === "e" || myName[i] === "i"  || myName[i] === "o" || myName[i] === "u") {
    vowels++;
  }
}
// console.log("Number of vowels in", myName, "is", vowels);

// for(let i = 0; i < myName.length; i++){
//     if (
//         myName[i] === "a" ||
//         myName[i] === "e" ||
//         myName[i] === "i" ||
//         myName[i] === "o" ||
//         myName[i] === "u"
//         ) {
//         console.log("*");;
//     } else {
//         console.log(myName[i]);
//     }
// }

for (let count = 0; count <= 20; count++){
    if(count % 2 === 0){
        continue;
    }
    console.log("Continue and Break: " + count);

    if(count > 10){
        break;
    }
}

let name1 = "alejandro";

for (let i = 0; i < name1.length; i++){
    
    console.log(name1[i]);

    if (name1[i].toLowerCase() === "a"){
        console.log("Continue");
        continue;
    }

    if (name1[i] == "d"){
        
        break;
    }
}