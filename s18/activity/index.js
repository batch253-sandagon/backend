// console.log("HellO!");

// addNum
function addNum(firstNum, secondNum){
    console.log("Displayed sum of " + firstNum + " and " + secondNum);
    console.log(firstNum + secondNum);
};
// subNum
function subNum(firstNum, secondNum){
    console.log("Displayed difference of " + firstNum + " and " + secondNum);
    console.log(firstNum - secondNum);
};

// Invocation
addNum(5, 15);
subNum(20, 5);

// multiplyNum Function
function multiplyNum(firstNum, secondNum){
    console.log("The product of " + firstNum + " and " + secondNum + ":");
    return firstNum * secondNum;
};

// Storing its value to a variable
let product = multiplyNum(50, 10);
// Displaying to console
console.log(product);

// divideNum Function
function divideNum(firstNum, secondNum){
    console.log("The quotient of " + firstNum + " and " + secondNum + ":");
    return firstNum / secondNum;
};

// Storing its value to a variable
let quotient = divideNum(50, 10);

// Displaying to console
console.log(quotient);

// getCircleAre Function
function getCircleArea(radius){
    console.log("The result of getting the area of a circle with " + radius + " radius:");
    const area = 3.1416 * (radius ** 2);
    return area;
};

const circleArea = getCircleArea(15);

// Displaying to console
console.log(circleArea);

// getAverage Function
function getAverage(score1, score2, score3, score4){
    console.log("The average of " + score1 + "," + score2+ "," + score3+ " and " + score4 + ":");
    const sum = score1 + score2 + score3 + score4;
    const average = sum / 4;
    return average;
};

// Storing the value to a variable
const averageVar = getAverage(20, 40, 60, 80);

// Displaying its value to the console
console.log(averageVar);

// checkIfPassed Function
function checkIfPassed(score, total){
    console.log("Is " + score + "/" + total + " a passing score?");
    const percentage = (score / total) * 100;
    const isPassed = percentage > 75;
    return isPassed;
};

// Storing value to a variable
const isPassingScore = checkIfPassed(38, 50);

// Displaying to the console
console.log(isPassingScore);

//Do not modify
//For exporting to test.js
try {
    module.exports = {
        addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
    }
} catch (err) {

}