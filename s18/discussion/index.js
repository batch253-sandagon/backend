// console.log("HellO!");

// addNum
function addNum(firstNum, secondNum){
    console.log("Displayed sum of " + firstNum + " and " + secondNum);
    console.log(firstNum + secondNum);
};
// subNum
function subNum(firstNum, secondNum){
    console.log("Displayed difference of " + firstNum + " and " + secondNum);
    console.log(firstNum - secondNum);
};

// Invocation
addNum(5, 15);
subNum(20, 5);

// multiplyNum Function
function multiplyNum(firstNum, secondNum){
    return firstNum * secondNum;
};

// divideNum Function
function divideNum(firstNum, secondNum){
    return firstNum / secondNum;
};

// Storing its value to a variable
let product = multiplyNum(50, 10);
let quotient = divideNum(50, 10);

// Displaying to console
console.log("The product of 50 and 10:");
console.log(product);
console.log("The quotient of 50 and 10:");
console.log(quotient);

// getCircleAre Function
function getCircleArea(radius){
    const area = 3.1416 * (radius ** 2);
    return area;
};

const circleArea = getCircleArea(15);

// Displaying to console
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);

// getAverage Function
function getAverage(score1, score2, score3, score4){
    const sum = score1 + score2 + score3 + score4;
    const average = sum / 4;
    return average;
};

// Storing the value to a variable
const averageVar = getAverage(20, 40, 60, 80);

// Displaying its value to the console
console.log("The average of 20,40,60 and 80:");
console.log(averageVar);

// checkIfPassed Function
function checkIfPassed(score, total){
    const percentage = (score / total) * 100;
    const isPassed = percentage > 75;
    return isPassed;
};

// Storing value to a variable
const isPassingScore = checkIfPassed(38, 50);

// Displaying to the console
console.log("Is 38/50 a passing score?");
console.log(isPassingScore);

//Do not modify
//For exporting to test.js
try {
    module.exports = {
        addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
    }
} catch (err) {

}