// Setup Dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute")

// Server Setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended : true})); // Read data from forms

// Database Connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin123@b253-sandagon.ocfyhy9.mongodb.net/s36?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection; 
// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

app.use("/tasks", taskRoute);

if(require.main === module){
    app.listen(port, () => console.log(`Server running at port ${port}...`))
}

// Use "module.exports" to export the router object to use in the "index.js"
module.exports = app;