// Contains all the endpoints for our application
// We separate the routes such that "app.js" only contains information on the server
// We need to use express' Router() function to achieve this
const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controller/taskController")

// Routes --------------------->
// The routes are responsible for defining the URIs (URI- Unified Resource Identifier) that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller 

// Route to get all the tasks
// This route expects to receive a GET request at the URL "/tasks"
// The whole URL is at "http://localhost:4000/tasks" this is because of the "/tasks" defined in the "index.js" that's applied to all routes in this file

router.get("/", (req, res)=>{
    // Invokes the "getAllTasks" function from the "taskController.js" file and sends the result back to the client/Postman
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/", (req, res)=>{
    taskController.createTask(req.body).then(result => res.send(result));
})

router.delete("/:id", (req, res)=>{
    taskController.deleteTask(req.params.id).then(result => res.send(result));
})

// Route to update a task
// This route expects to receive a PUT request at the URL "/tasks/:id"
// The whole URL is at "http://localhost:4000/tasks/:id"
router.put("/:id", (req, res)=>{
    taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

router.get("/:id", (req, res)=>{
    taskController.getSpecificTask(req.params.id).then(result => res.send(result));
})

router.put("/:id/complete", (req, res)=>{
    taskController.updateSpecificTask(req.params.id).then(result => res.send(result));
})

module.exports = router;