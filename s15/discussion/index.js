// Comments:

// Comments are parts of the code that gets ignored

/*
    There are two types of comments:
        1. Single-line Comment - denoted by two slashes
        2. Multi-line Comment - denoted by a forward slash and a two asterisks.

*/

console.log("Hello Batch 253!");

/* 

    Syntax - this is the grammar of a language
    Statements - this is the commands or instructions for the computer

*/

// Variable Section

// It is used to contain data.
// Any information that is used by an application is stored in what we call a memory.