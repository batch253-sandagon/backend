// console.log("?");

/*
    OBJECTS 
        - is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.
*/

// Object Literals
    /*
        - one of the methods in creating obhects

        Syntax: 
            let onjectName = {
                keyA/propertyA: valueA,
                keyB/propertyB: valueB,
            }
*/

let cellphone = {
    name: "Nokia 3210",
    manufactureDate: 1999,
}

console.log("Result from creating object using Object Literals: ");
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects Using a Constructor Function (Object Constructor)
/* 
// Creating Objects Using a Constructor Function (Object Constructor)
/*
    Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

    NOTE: when creating an object using constructor function the naming convention will on PasxcalCase

    Syntax:
        function ObjectName(param_valuea, param_valueB){
            this.keyA = valueA,
            this.keyB = valueB
        };

        let variableName = new function ObjectName(args_valueA, args_valueB);

        console.log(variableName);

        Note: Do not forget the "new" keyword/reserved word when creating a new object
*/


function Laptop(brand, manufactureDate){
    this.brand = brand;
    this.manufactureDate = manufactureDate
};

let laptopRonald = new Laptop("Acer", 2020);
console.log(laptopRonald);

let laptopMin = new Laptop("MacBook Air", [2020, 2021]);
console.log(laptopMin);

let laptop = Laptop("MacBook Air", [2020, 2021]);
console.log(laptop);

// Create empty objects as placeholder
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

/*
	Mini Activity:
		- Create an object constructor function to produce 2 objects with 3 key-value pairs.
		- Log the 2 new objects in the console and send SS in our batch hangout.
*/

function Chair(brand, type){
    this.brand = brand,
    this.type = type
};

let chairOne = new Chair("Uratex", "Monoblock Chair");
console.log(chairOne);

let chairTwo = new Laptop("Brand Y", "Gaming Chair");
console.log(chairTwo);

// Accessing Object Properties
// using dot nation
console.log("Resulting from dot notation: " + laptopRonald.brand);
// using square bracket notation
console.log("Resulting from square bracket notation: " + laptopRonald["brand"]);

// Accessing array objs
let deviceArr = [laptopRonald, laptopMin]
console.log(deviceArr);

// dot notation
console.log(deviceArr[0].manufactureDate);
// square bracket notation
console.log(deviceArr[0]["manufactureDate"]);

// Initializing/Adding/Deleting/Reassigning Object Properties
// CRUD OPERATIONS

//Initializing Object
let car = {};
console.log(car);

// Adding Object Properties
car.name = "Honda Civic";
console.log("Results from adding properties using dot notation: ");
console.log(car);
/*
		    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
		    - This also makes names of object properties to not follow commonly used naming conventions for them
*/

car["manufacture date"] = 2019;
console.log(car["manufacture date"]);
console.log("Results from adding properties using dot notation: ");
console.log(car);

// Deleting Object Properties
delete car["manufacture date"];
console.log("Result from deleting properties: ");
console.log(car);

// Reassigning Object Properties (Update)
car.name = "Tesla";
console.log("Result from reassigning properties: ");
console.log(car);

// Object Methods
/*
	This method is a function which is stored in an object property. Ther are also functions and one of the key difference that they have is that methods are functions related to a specific object.
*/

let person = {
    name: "Pedro",
    age: 25,
    talk: function(){
        console.log("Hello. Ako si " + this.name)
    }
}

console.log(person);
console.log("Result");
person.talk();

person.walk = function(){
    console.log(this.name + " have walked 25 steps forward.");
}

person.walk();

let friend = {
    firstName: "Maria",
    lastName: "Dela Cruz",
    address: {
        city: "Austin, Texas",
        country: "US"
    },
    emails: ["maria143@mail.com", "maria_bosxz@mail.com"],
    introduce: function(){
        console.log("Hello! My name is " + this.firstName + " " + this.lastName + ". " + "I live in " + this.address.city + ", " + this.address.country + ".");
    }
}

friend.introduce();

let myPokemon = {
    name: "Charizard",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
    },
    faint: function(){
        console.log("Pokemon fainted.");
    }
}

console.log(myPokemon);
myPokemon.tackle();
myPokemon.faint();

// Creating an object constructor instead will help with this process
function Pokemon(name, level){
    // Properties
    this.name = name,
    this.level = level,
    this.health = 2 * level,
    this.attack = level,
    // Methods
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s health is now reduced to " + target.health);
    },
    this.faint = function(){
        console.log(this.name + " fainted.");
    }
}

// Creates new instances of the "Pokemon" object each with their unique properties

let pikachu = new Pokemon("Pikachu", 16);
let charmander = new Pokemon("Charmander", 8);

pikachu.tackle(charmander);