// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

let trainer = {};

// Initialize/add the given object properties and methods

// Properties

trainer.name = "Ash Ketchum";
trainer.age = 10;
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"]
trainer.friends = {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"]
};

console.log(trainer);

// Methods

trainer.talk = function(){
    return "Pikachu! I choose you!"
}

// Check if all properties and methods were properly added

// Access object properties using dot notation

console.log("Result of dot notation:");
console.log(trainer.name);

// Access object properties using square bracket notation

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// Access the trainer "talk" method

console.log("Result of talk method");
console.log(trainer.talk());

// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
    this.tackle = function(targetPokemon){
        console.log(this.name + " tackled " + targetPokemon.name);
            targetPokemon.health -= this.attack;
            console.log(targetPokemon.name + "'s health is now reduced to " + targetPokemon.health);
        if(targetPokemon.health <= 0){
            targetPokemon.faint(targetPokemon.name);
        }
    };
    this.faint = function(targetPokemon){
        console.log(targetPokemon + " has fainted.");
    }
};

// Create/instantiate a new pokemon

const pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

// Create/instantiate a new pokemon

const geodude = new Pokemon("Geodude", 8);
console.log(geodude);

// Create/instantiate a new pokemon

const mewtwo = new Pokemon("Mewtwo", 100); // OP!
console.log(mewtwo);

// Invoke the tackle method and target a different object

geodude.tackle(pikachu);
console.log(pikachu);

// Invoke the tackle method and target a different object

mewtwo.tackle(geodude); // OP 100% HAHAHA
console.log(geodude);


//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}