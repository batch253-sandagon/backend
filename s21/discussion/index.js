// console.log("?");

// ARRAY

let studentNumberA = "2023-1921";
let studentNumberB = "2023-1922";
let studentNumberC = "2023-1923";
let studentNumberD = "2023-1924";

let studentNumbers = ["2023-1921", "2023-1922", "2023-1923", "2023-1924"];

/*
	- Arrays are used to store multiple related values in a single variable
	- They are declared using square brackets ([]) also known as "Array Literals"
	- Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks
	- Arrays also provide access to a number of functions/methods that help in achieving this
	- A method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
    - Majority of methods are used to manipulate information stored within the same object
	- Arrays are also objects which is another data type
	- The main difference of arrays with an object is that it contains information in a form of a "list" unlike objects which uses "properties"
*/

// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "MSI", ]

// Mixed
let mixedArray = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArray);

// Alternative
let myTasks = [
    "html",
    "js",
    "css",
    "sass"
]

let city1 = "Tokyo", city2 = "Manila", city3 = "Berlin";

let cities = [city1, city2, city3]

console.log(myTasks);
console.log(cities);

// [SECTION] length property

console.log(myTasks.length);
console.log(cities.length);

let fullName = "Sana Minatozaki";
console.log(fullName.length);

let blankArray = [];
console.log(blankArray.length);
console.log(blankArray);

// Deleting last item
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// using decrementation
cities.length--;
console.log(cities);

// We can't delete using .length property in strings
fullName.length--;
console.log(fullName);

// Lengthen Array
let theBeatles = ["John", "Paul", "Ringo", "George", ];
console.log(theBeatles);

theBeatles.length++;
console.log(theBeatles.length);

// // Assigning a value to a specific array index
console.log(theBeatles);
theBeatles[4] = "Jimbo";
console.log(theBeatles);

console.log(theBeatles[2]);
console.log(myTasks[0]);
console.log(grades[20]);

grades[20] = 90.5;
console.log(grades);

let koponanNiEugene = ["Eugene", "Vincent", "Alfred", "Dennis", ];
console.log(koponanNiEugene[3]);
console.log(koponanNiEugene[1]);

let member = koponanNiEugene[2];
console.log(member);

console.log("Array before reassignment: " + koponanNiEugene);

koponanNiEugene[2] = "Jeremiah";
console.log("Array after reassignment: " + koponanNiEugene);

let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegend.length - 1;
console.log(bullsLegend[lastElementIndex]);
console.log(bullsLegend[bullsLegend.length - 1]);

// Adding items into the array
let newArr = [];
console.log(newArr);
console.log(newArr[0]);
newArr[0] = "Tzuyu";
console.log(newArr);
newArr[1] = "Mina";
console.log(newArr);

// Adding at the end
console.log(newArr.length);
newArr[newArr.length] = "Momo";
console.log(newArr);
console.log(newArr.length);

superHeroes = [];

function addHero(hero){
    superHeroes[superHeroes.length] = hero;
}

addHero("Spiderman");
addHero("Thor");
addHero("Ironman");
console.log(superHeroes);

function searchHero(index){
    return superHeroes[index];
}

let heroFound = searchHero(0);
console.log(heroFound);

console.log("-------");

for(let index = 0; index < newArr.length; index++){
    console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 50, 98];
for(let index = 0; index < numArr.length; index++){
    if(numArr[index] % 5 === 0){
        console.log(numArr[index] + " is divisible by 5.");
    } else {
        console.log(numArr[index] + " is not divisible by 5.");
    };
};

// Multidimentional Array

let chessBoard = [
    ["a1", "b1", "c1", "d1", "e1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "g8", "h8"],
];

console.log(chessBoard);
console.log(chessBoard[1][4]);
console.log("Pawn moves to : " + chessBoard[7][4]);