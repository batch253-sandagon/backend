// console.log("Hello World!");
/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:
    // Declaration:
    function printUserInfo(){

        let fullName = "John Doe";
        console.log("Hello, I'm " + fullName + ".");

        let age = "25";
        console.log("I am " + age + " years old.");

        let location = "123 street, Quezon City";
        console.log("I live in " + location);

        let catName = "Joe";
        console.log("I have a cat named " + catName + ".");

        let dogName = "Danny";
        console.log("I have a dog named " + dogName + ".");
    };

    //Invocation:
    printUserInfo();

/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:
    // Declaration:
    function printFiveBands(){
        let firstBand = "The Beatles";
        let secondBand = "Taylor Swift";
        let thirdBand = "The Eagles";
        let fourthBand = "Rivermaya";
        let fifthBand = "Eraserheads";

        console.log(firstBand);
        console.log(secondBand);
        console.log(thirdBand);
        console.log(fourthBand);
        console.log(fifthBand);
    };

    //Invocation:
    printFiveBands();

/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:
    // Declaration:
    function printFiveMovies(){
        let firstMovie = "Lion King";
        let secondMovie = "Howl's Moving Castle";
        let thirdMovie = "Meet the Robinsons";
        let fourthMovie = "School of Rock";
        let fifthMovie = "Sprited Away";

        console.log(firstMovie);
        console.log(secondMovie);
        console.log(thirdMovie);
        console.log(fourthMovie);
        console.log(fifthMovie);
    }

    //Invocation:
    printFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1);
    console.log(friend2);
    console.log(friend3);
	// console.log(true); 
	// console.log(friends); 
};

printFriends();










//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}