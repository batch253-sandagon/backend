// Inserting a single room
db.rooms.insertOne({
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "roomsAvailable": 10,
    "isAvailable": false
});

// Inserting multiple rooms
db.rooms.insertMany([
    {
        "name": "double",
        "accomodates": 3,
        "price": 2000,
        "description": "A room fit for a small family going on a vacation",
        "roomsAvailable": 5,
        "isAvailable": false 
    },
    {
        "name": "queen",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with a queen sized bed perfect for a simple getaway",
        "roomsAvailable": 15,
        "isAvailable": false
    }
]);

// Searching for a room with the name double
db.rooms.findOne({"name": "double"});

// Updating the queen room and set the available rooms to 0
db.rooms.updateOne(
    {
      "name": "queen"  
    },
    {
        $set: {
            "roomsAvailable": 0 
        }
    }
);

// Deleting all rooms that have 0 availability
db.rooms.deleteMany(
    {
        "roomsAvailable": 0
    }
);

// Searching for all hotel rooms
db.rooms.find();