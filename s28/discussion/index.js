// CRUD Operations
    /*
        - CRUD operations are the heart of any backend application.
        - Mastering the CRUD operations is essential for any developer.
        - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
        - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
    */

//     Insert One Document
//     db.collectionName.insertOne({
//         "fieldA": "valueA",
//         "fieldB": "valueB"
//     });

// Insert Many Documents
//     db.collectionName.insertMany([
//         {
//             "fieldA": "valueA",
//             "fieldB": "valueB"
//         },
//         {
//             "fieldA": "valueA",
//             "fieldB": "valueB"
//         }
//     ]);   
     
db.users.insertOne({
    "firstName": "Ronald",
    "lastName": "Sandagon",
    "mobileNumber": "+639123456789",
    "email": "sandagonronald@mail.com",
    "company": "zuitt"
})

db.users.Insertany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "hawkingstephen@mail.com"
        },
        courses: ["Python", "React", "PHP", "CSS"],
        department: "none"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "98765432",
            email: "armstrongneil@mail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }
]);

// Finding docs (Read/Retrive)
/*
    Syntax:
        - db.collectionName.find() - find all
        - db.collectionName.find({field: value}); - all document that will match the criteriea
        - db.collectionName.findOne({field: value}) - first document that will mathc the criteria
        - db.collectionName.findOne({}) - find first document
*/
db.users.find();
db.users.find({"firstName": "Stephen"});

db.courses.insertMany([
    {
        "name": "JavaScript",
        "price": 5000,
        "description": "Introduction to Javascript",
        "issActive": true
    },
    {
        "name": "HTML 101",
        "price": 2000,
        "description": "Introduction to HTML",
        "isActive": true
    }
]);

db.courses.find();
db.courses.findOne({});

// Updating/Replacing/Modifying docs (Update)
/*
    Syntax:
    Updating One doc
        db.collectionName.updateOne(
            {
                criteria: value
            },
            {
                $set: {
                    fieldToBeUpdated: value
                }
            }
        );
        - update the first match
    
    Multiple/Many docs
    db.collectionName.updateMany({
        {
            field: value
        },
        {
            $set: {
                fieldToBeUpdated: value
            }
        }
    });
    update mutiple docs that matched the criteria
*/

db.users.insertOne({
    "firstName": "Test",
    "lastName": "Test",
    "mobileNumber": "+639123456789",
    "email": "test@mail.com",
    "company": "test"
});

db.users.updateOne(
    {
        "firstName": "Test"
    },
    {
        $set: {"firstName": "Bill",
        "lastName": "Gates",
        "mobileNumber": "123456789",
        "email": "bilgates@mail.com",
        "company": "Microsoft",
        "status": "active"}
    }
);

db.courses.updateOne(
    {
        "name": "HTML 101"
    },
    {
        $set: {
        "ssActive": false}
    }
);

db.courses.updateMany(
    {},
    {
        $set:{
            "enrolles": 10
        }
    }
);

db.courses.updateMany(
    {},
    {
        $rename:{
            "dept": "dept"
        }
    }
);

// Remove
db.users.updateOne(
    {
        "firstName": "Bill"
    },
    {
        $set: {
        "status": "active"}
    }
);

db.users.deleteOne({
    "firstName": "Stephen"
})

db.users.deleteMany({});